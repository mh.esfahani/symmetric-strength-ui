import {NgModule} from "@angular/core";
import {ButtonModule} from "primeng/button";

@NgModule(
  {
    declarations:[],
    exports:[],
    imports:[
      ButtonModule,
    ]
  }
)
export class SharedModule {
}
